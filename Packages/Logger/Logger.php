<?php


namespace GfRedirects\Logger;

class Logger
{
    /**
     * @param $msg
     */
    public static function log($msg): void
   {
       $file = WP_CONTENT_DIR . '/uploads/redirectLog.txt';
       $msg = date('Y/m/d H:i:s') . ' : ' . $msg;
       $data = file_get_contents($file) . PHP_EOL . PHP_EOL . $msg;
       if (!file_put_contents($file, $data)){
           $to = TICKETING_EMAIL;
           $subject = '300 Redirects logger error';
           $body = 'Greska u snimanju log fajla. Entry je: '.$msg;
           $headers = array('Content-Type: text/html; charset=UTF-8');
           wp_mail( $to, $subject, $body, $headers);
       }
   }
}