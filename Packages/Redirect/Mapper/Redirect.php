<?php


namespace GfRedirects\Redirect\Mapper;


use Exception;
use GfRedirects\Redirect\Model\Redirect as Model;

class Redirect
{
    private \wpdb $db;
    private string $tableName;


    /**
     * Redirect constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
        $this->tableName = $this->db->prefix . REDIRECT_TABLE_NAME;
    }

    /**
     * @param Model $model
     * @return int
     * @throws Exception
     */
    public function insert(Model $model): int
    {
        if ($this->db->insert($this->tableName, [
            'from' => $model->getFrom(),
            'to' => $model->getTo(),
            'statusCode' => $model->getStatusCode(),
            'active' => $model->getActive(),
            'userId' => $model->getUserId()
        ])) {
            return $this->db->insert_id;
        }
        throw new Exception($this->db->last_error);
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @param array $filters
     * @return array|object|null
     */
    public function getAll(int $offset = null, int $limit = null, array $filters = [])
    {
        $sql = "SELECT * FROM $this->tableName";
        if (count($filters) !== 0) {
            foreach ($filters as $arg) {
                if (isset($arg['search'])) {
                    $i = 0;
                    foreach ($arg['search'] as $name => $value) {
                        if ($i === 0) {
                            $sql .= " WHERE `{$name}` LIKE '%{$value}%'";
                        } else {
                            $sql .= " OR `{$name}` LIKE '%{$value}%'";
                        }
                        $i++;
                    }
                }
                if (isset($filters['order'])) {
                    foreach ($filters['order'] as $name => $value) {
                        $sql .= " ORDER BY `{$name}` {$value}";
                    }
                }
            }
        } else {
            $sql .= " ORDER BY `createdAt` DESC";
        }
        if ($limit) {
            $sql.= " LIMIT $limit";
        }
        if ($offset) {
            $sql.= " OFFSET $offset";
        }
        return $this->db->get_results($sql, ARRAY_A);
    }

    public function getTotalCount(array $filters = [])
    {
        $sql = "SELECT COUNT(*) FROM $this->tableName";
        if (count($filters) !== 0) {
            foreach ($filters as $arg) {
                if (isset($arg['search'])) {
                    $i = 0;
                    foreach ($arg['search'] as $name => $value) {
                        if ($i === 0) {
                            $sql .= " WHERE `{$name}` LIKE '%{$value}%'";
                        } else {
                            $sql .= " OR `{$name}` LIKE '%{$value}%'";
                        }
                        $i++;
                    }
                }
            }
        }
        return $this->db->get_results($sql, ARRAY_N)[0];
    }

    /**
     * @param string $field
     * @param $value
     * @return array|mixed|object|null
     */
    public function getBy(string $field, $value)
    {
        $sql = "SELECT * FROM $this->tableName WHERE `{$field}` = '{$value}'";
        $result = $this->db->get_results($sql, ARRAY_A);
        if (count($result) > 0) {
            return $result[0];
        }
        return $result;
    }

    /**
     * @param Model $model
     * @return int
     * @throws Exception
     */
    public function update(Model $model): int
    {
        if ($this->db->update($this->tableName, [
            'from' => $model->getFrom(),
            'to' => $model->getTo(),
            'statusCode' => $model->getStatusCode(),
            'active' => $model->getActive(),
            'userId' => $model->getUserId()
        ], ['redirectId' => $model->getId()])) {
            return $this->db->insert_id;
        }
        throw new Exception($this->db->last_error);
    }

    /**
     * @param Model $model
     * @return int
     * @throws Exception
     */
    public function delete(Model $model): int
    {
        if ($this->db->delete($this->tableName, ['redirectId' => $model->getId()])) {
            return $this->db->insert_id;
        }
        throw new Exception($this->db->last_error);
    }

}