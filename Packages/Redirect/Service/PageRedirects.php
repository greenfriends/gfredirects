<?php


namespace GfRedirects\Redirect\Service;


use GfRedirects\Redirect\Controller\RedirectController;

class PageRedirects
{
    private RedirectController $redirectController;

    public function __construct()
    {
        $this->addActions();
        $this->redirectController = new RedirectController();
    }

    private function addActions(): void
    {
        add_action('template_redirect', [$this, 'redirectPage']);
    }

    /**
     * Check for redirect in wp_gf_redirects table and if found redirect user to url specified in from column
     */
    public function redirectPage(): void
    {
        $currentUrl = home_url($_SERVER['REQUEST_URI']);
        $redirect = $this->redirectController->getBy('from', $currentUrl);
        if ($redirect){
            exit(wp_redirect($redirect->getTo(), $redirect->getStatusCode(),'gfRedirects'));
        }
    }
}