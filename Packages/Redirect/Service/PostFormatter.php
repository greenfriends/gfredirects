<?php


namespace GfRedirects\Redirect\Service;


class PostFormatter
{
    public static function formatData(array $data)
    {
        if ($data['redirectId'] !== '') {
            $redirectId = $data['redirectId'];
        }
        return [
            'from' => $data['from'],
            'to' => $data['to'],
            'statusCode' => $data['statusCode'],
            'active' => $data['active'],
            'userId' => get_current_user_id(),
            'redirectId' => $redirectId ?? null
        ];
    }
}