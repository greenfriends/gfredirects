<?php


namespace GfRedirects\Redirect\Service;


use GfRedirects\Redirect\Controller\RedirectController;
use GfRedirects\Redirect\Model\Redirect;

class AutoRedirect
{
    private RedirectController $redirectController;

    public function __construct()
    {
        $this->addFilters();
        $this->redirectController = new RedirectController();
    }

    private function addFilters(): void
    {
        add_filter('wp_insert_post_data', [$this, 'createAutoRedirectProducts'], 99, 2);
        add_filter('wp_update_term_data', [$this, 'createAutoRedirectCategory'], 99, 3);
    }

    /**
     * Detects when user changes slug or category of products and creates redirect from old to new url
     * @param array $data
     * @param array $postArr
     * @return array
     */
    public function createAutoRedirectProducts(array $data, array $postArr): array
    {
        //If is doing auto-save
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $data;
        }
        //If is doing auto-save via AJAX
        if (defined('DOING_AJAX') && DOING_AJAX) {
            return $data;
        }
        //We make redirect only for published post
        if ($postArr['post_status'] !== 'publish') {
            return $data;
        }
        //Only trigger on update of products
        if (!isset($postArr['save'])) {
            return $data;
        }

        if (count($postArr) > 0) {
            unset($postArr['tax_input']['product_cat'][0]);  //It is always 0 and useless
            if ($postArr['post_type'] === 'product') {
                $newPermalink = '';
                $product = wc_get_product($postArr['ID']);
                $oldPermalink = $product->get_permalink();
                $catsToIgnore = $this->getIgnoredCats();
                if ($product->get_slug() !== $data['post_name']) {
                    $newPermalink = str_replace($product->get_slug(), $data['post_name'], $oldPermalink);
                }
                if (count(array_diff($postArr['tax_input']['product_cat'], $product->get_category_ids())) > 0 ||
                    count(array_diff($product->get_category_ids(), $postArr['tax_input']['product_cat'])) > 0) {
                    foreach ($postArr['tax_input']['product_cat'] as $key => $catId){
                        if (in_array($catId, $catsToIgnore)){
                            unset($postArr['tax_input']['product_cat'][$key]);
                        }
                    }
                    $newPermalink = get_home_url();
                    $cats = get_terms([
                        'taxonomy' => 'product_cat',
                        'include' => $postArr['tax_input']['product_cat'],
                        'fields' => 'slugs'
                    ]);
                    //To avoid creating urls with specijalne promocije or akcija cats we use switch instead of array_key_last
                    switch (count($cats)){
                        case 0:
                            $newPermalink.= '/uncategorized/' . $data['post_name'] . '/';
                            break;
                        case 1:
                            $newPermalink.= '/' . $cats[0].'/' . $data['post_name'] . '/';
                            break;
                        case 2:
                            $newPermalink.= '/' . $cats[1].'/' . $data['post_name'] . '/';
                            break;
                        default:
                            $newPermalink.= '/' . $cats[2].'/' . $data['post_name'] . '/';
                    }
                }
                if ($newPermalink !== '' && $newPermalink !== $oldPermalink) {
                    $redirect = $this->redirectController->getBy('from', $newPermalink);
                    if ($redirect && $this->detectTwoWayRedirect($oldPermalink, $redirect)){
                        $this->redirectController->deleteRedirect($redirect);
                       return $data;
                    }
                    $this->redirectController->createRedirect([
                        'from' => $oldPermalink,
                        'to' => $newPermalink,
                        'statusCode' => 301,
                        'active' => 1,
                        'userId' => get_current_user_id()
                    ]);
                }
            }
        }

        return $data;
    }

    /**
     * Detects when user changes slug of category and creates redirect from old to new url
     * @param array $data
     * @param int $termId
     * @param string $taxonomy
     * @return array
     */
    public function createAutoRedirectCategory(array $data, int $termId, string $taxonomy): array
    {
        if ($taxonomy !== 'product_cat'){
            return $data;
        }
        $oldCat = get_term($termId);
        if ($data['slug'] !== $oldCat->slug){
            $catProductsIds = wc_get_products([
                'category' => [$oldCat->slug],
                'numberposts' => '-1',
                'return' => 'ids'
            ]);
            foreach ($catProductsIds as $productId){
                $productPermalink = get_permalink($productId);
                $newPermalink = str_replace($oldCat->slug, $data['slug'], $productPermalink);
                if ($newPermalink !== $productPermalink){
                    $redirect = $this->redirectController->getBy('from', $newPermalink);
                    if ($redirect && $this->detectTwoWayRedirect($productPermalink, $redirect)){
                        $this->redirectController->deleteRedirect($redirect);
                        return $data;
                    }
                    $this->redirectController->createRedirect([
                        'from' => $productPermalink,
                        'to' => $newPermalink,
                        'statusCode' => 301,
                        'active' => 1,
                        'userId' => get_current_user_id()
                    ]);
                }
            }
        }
        return $data;
    }

    /**
     * @return array
     */
    private function getIgnoredCats(): array
    {
        $specCatId = get_terms([
            'taxonomy' => 'product_cat',
            'name__like' => 'Specijalne promocije',
            'fields' => 'ids'
        ])[0];
        $catsToIgnore[] = $specCatId;
        foreach (get_term_children($specCatId,'product_cat') as $catId){
            $catsToIgnore[] = $catId;
        }
        return $catsToIgnore;
    }

    /**
     *
     * @param string $from
     * @param Redirect $redirect
     * @return bool
     */
    private function detectTwoWayRedirect(string $from, Redirect $redirect): bool
    {
        return $redirect->getTo() === $from;
    }
}