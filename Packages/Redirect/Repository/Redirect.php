<?php


namespace GfRedirects\Redirect\Repository;


use Exception;
use GfRedirects\Logger\Logger;
use GfRedirects\Redirect\Mapper\Redirect as Mapper;
use GfRedirects\Redirect\Model\Redirect as Model;

class Redirect
{
    private Mapper $mapper;

    /**
     * Redirect constructor.
     */
    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param array $data [
     * 'from' => string,
     * 'to' => string,
     * 'statusCode' => string,
     * 'id' => int|null,
     * 'active' => int|null,
     * 'userId' => int|null,
     * 'createdAt' => string|null,
     * 'updatedAt' => string|null]
     * @return int|null
     */
    public function create(array $data): ?int
    {
        try {
            $result = $this->mapper->insert($this->make($data));
            if ($result) {
                /*
                 * if another redirect is already redirecting to this redirect from then to prevent multiple redirect we
                 * change that redirects to address to this new to address.
                */
                $redirects = $this->checkIfFromUrlExistsInToUrl($data['from']);
                foreach ($redirects as $redirect){
                        $updateData = [];
                        $updateData['from'] = $redirect->getFrom();
                        $updateData['to'] = $data['to'];
                        $updateData['statusCode'] = $redirect->getStatusCode();
                        $updateData['redirectId'] = $redirect->getId();
                        $updateData['active'] = $redirect->getActive();
                        $updateData['userId'] = $redirect->getUserId();
                        $updateData['createdAt'] = $redirect->getCreatedAt();
                        $updateData['updatedAt'] = $redirect->getUpdatedAt();
                        $this->update($updateData);
                }
            }
            return $result;
        } catch (Exception $e) {
            Logger::log($e->getMessage());
            return null;
        }
    }

    /**
     * Checks if some other redirect is already redirecting to passed url
     * @param string $from
     * @return Model[]|null
     */
    private function checkIfFromUrlExistsInToUrl(string $from): ?array
    {
        $redirects = $this->getAll(null, null, [['search' => ['to' => $from]]]);
        if ($redirects) {
            return $redirects;
        }
        return null;
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @param array $filters
     * @param bool $ajax
     * @return Model[]|null
     */
    public function getAll(int $offset = null, int $limit = null, array $filters = [], bool $ajax = false): ?array
    {
        $data = [];
        $redirects = $this->mapper->getAll($offset, $limit, $filters);
        foreach ($redirects as $redirect) {
            if ($ajax === true) {
                $data[] = $redirect;
                continue;
            }
            $data[] = $this->make($redirect);
        }
        if (count($data) > 0) {
            return $data;
        }
        return null;
    }

    /**
     * @param string $field
     * @param $value
     * @return Model|null
     */
    public function getBy(string $field, $value): ?Model
    {
        $data = $this->mapper->getBy($field, $value);
        if (count($data) > 0) {
            return $this->make($data);
        }
        return null;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data): bool
    {
        if ($data['from'] === $data['to']) {
            return false;
        }
        try {
            $this->mapper->update($this->make($data));
            $redirects = $this->checkIfFromUrlExistsInToUrl($data['from']);
            foreach ($redirects as $redirect){
                $updateData = [];
                $updateData['from'] = $redirect->getFrom();
                $updateData['to'] = $data['to'];
                $updateData['statusCode'] = $redirect->getStatusCode();
                $updateData['redirectId'] = $redirect->getId();
                $updateData['active'] = $redirect->getActive();
                $updateData['userId'] = $redirect->getUserId();
                $updateData['createdAt'] = $redirect->getCreatedAt();
                $updateData['updatedAt'] = $redirect->getUpdatedAt();
                $this->update($updateData);
            }
            return true;
        } catch (Exception $e) {
            Logger::log($e->getMessage());
            return false;
        }
    }

    /**
     * @param Model $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        try {
            $this->mapper->delete($model);
            return true;
        } catch (Exception $e) {
            Logger::log($e->getMessage());
            return false;
        }
    }

    public function make(array $data): Model
    {
        return new Model(
            $data['from'],
            $data['to'],
            $data['statusCode'],
            $data['redirectId'] ?? null,
            $data['active'] ?? 0,
            $data['userId'] ?? null,
            $data['createdAt'] ?? null,
            $data['updatedAt'] ?? null
        );
    }

    public function getTotalCount(array $filters = [])
    {
        return $this->mapper->getTotalCount($filters);
    }
}