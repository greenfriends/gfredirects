<?php


namespace GfRedirects\Redirect\Model;


class Redirect
{
    private ?int $id;
    private string $from;
    private string $to;
    private string $statusCode;
    private int $active;
    private ?int $userId;
    private ?string $createdAt;
    private ?string $updatedAt;

    /**
     * Redirect constructor.
     * @param string $from
     * @param string $to
     * @param string $statusCode
     * @param int|null $id
     * @param int $active
     * @param int|null $userId
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(
        string $from,
        string $to,
        string $statusCode,
        int $id = null,
        int $active = 0,
        int $userId = null,
        string $createdAt = null,
        string $updatedAt = null
    ) {
        $this->id = $id;
        $this->from = $from;
        $this->to = $to;
        $this->statusCode = $statusCode;
        $this->active = $active;
        $this->userId = $userId;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }
}