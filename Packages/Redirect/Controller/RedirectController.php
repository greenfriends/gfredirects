<?php


namespace GfRedirects\Redirect\Controller;


use GfRedirects\Redirect\Mapper\Redirect as Mapper;
use GfRedirects\Redirect\Model\Redirect;
use GfRedirects\Redirect\Repository\Redirect as Repository;

class RedirectController
{
    private Repository $repo;

    /**
     * RedirectController constructor.
     */
    public function __construct()
    {
        $this->repo = new Repository(new Mapper());
    }

    /**
     * @param array $data [
     * 'from' => string,
     * 'to' => string,
     * 'statusCode' => string,
     * 'id' => int|null,
     * 'active' => int|null,
     * 'userId' => int|null,
     * 'createdAt' => string|null,
     * 'updatedAt' => string|null]
     */
    public function createRedirect(array $data): array
    {
        if ($this->repo->create($data)){
            return ['success'=>'Snimanje uspešno završeno'];
        }

        return ['error'=> 'Došlo je do greške prilikom snimanja'];
    }

    /**
     * @param array $data
     * @return array
     */
    public function getAll(array $data): array
    {
        $filters = [];
        $offset = $data['start'];
        $limit = $data['length'];
        $searchValue = $data['search']['value'] ?? '';
        $totalCount = $this->repo->getTotalCount();
        $totalCountWithFilters = $totalCount;
        if ($searchValue !== '') {
            $filters[] = ['search' => ['from' => $searchValue, 'to' => $searchValue]];
            $totalCountWithFilters = $this->repo->getTotalCount($filters);
        }

        return [
            'draw' => (int)$_POST['draw'],
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $totalCountWithFilters,
            'data' => $this->repo->getAll($offset, $limit, $filters,true)
        ];
    }

    /**
     * @param string $field
     * @param $value
     */
    public function getBy(string $field, $value): ?Redirect
    {
        return $this->repo->getBy($field, $value);
    }

    /**
     * @param array $data
     * @return string[]
     */
    public function editRedirect(array $data): array
    {
        if ($this->repo->update($data)){
            return ['success'=>'Snimanje uspešno završeno'];
        }

        return ['error'=> 'Došlo je do greške prilikom snimanja'];
    }

    /**
     * @param Redirect $model
     * @return string[]
     */
    public function deleteRedirect(Redirect $model): array
    {
        if ($this->repo->delete($model)){
            return ['success'=>'Uspešno ste obrisali redirect'];
        }
        return ['error'=> 'Došlo je do greške prilikom brisanja'];
    }
}