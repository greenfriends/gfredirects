<?php
/** @noinspection ProperNullCoalescingOperatorUsageInspection */


namespace GfRedirects\MenuPage\Controller;


use GfRedirects\Redirect\Controller\RedirectController;
use GfRedirects\Redirect\Model\Redirect;
use GfRedirects\Redirect\Service\PostFormatter;

class MenuPageController
{
    private RedirectController $redirectController;

    /**
     * MenuPageController constructor.
     */
    public function __construct()
    {
        $this->redirectController = new RedirectController();
    }

    public function handlePageAction(string $action = null): void
    {
        switch ($action) {
            case 'redirectCreateForm':
                $id = $_GET['redirectId'] ?? null;
                if ($id){
                    $redirectEntry = $this->redirectController->getBy('redirectId', $id);
                }
                $this->getNav('redirectCreate');
                $this->redirectForm($redirectEntry ?? null);
                break;
            case 'createRedirect':
                if (count($_POST) > 0 ){
                    $msg = $this->redirectController->createRedirect(PostFormatter::formatData($_POST));
                }
                $this->getNav('redirectList',$msg ?? '');
                $this->main();
                break;
            case 'editRedirect':
                if (count($_POST) > 0 ){
                    $msg = $this->redirectController->editRedirect(PostFormatter::formatData($_POST));
                }
                $this->getNav('redirectList',$msg ?? '');
                $this->main();
                break;
            case 'deleteRedirect':
                if (count($_POST) > 0){
                    $msg = $this->redirectController->deleteRedirect($this->redirectController->getBy('redirectId',$_POST['redirectId']));
                }
                $this->getNav('redirectList',$msg ?? '');
                $this->main();
                break;
            default :
                $this->getNav('redirectList');
                $this->main();
        }
    }

    public function handleAjaxAction(): void
    {
        switch ($_POST['method']) {
            case 'getAll':
                wp_send_json($this->redirectController->getAll($_POST));
                break;
        }
    }

    public function setupAjaxHandler(): void
    {
        add_action('wp_ajax_redirectAdminPage', [$this, 'handleAjaxAction']);
    }

    private function main(): void
    {
        require_once GF_REDIRECTS_DIR . 'Packages/MenuPage/view/templates/redirectList.html';
    }

    private function redirectForm(Redirect $model = null): void
    {
        $title = 'Create redirect';
        $formAction = '?page=gf-redirects&action=createRedirect';
        if ($model){
            $title = 'Edit redirect';
            $formAction = '?page=gf-redirects&action=editRedirect';
            $from = $model->getFrom();
            $to = $model->getTo();
            if ($model->getStatusCode() === '301'){
                $selected301 = 'selected';
            }
            if ($model->getStatusCode() === '302'){
                $selected302 = 'selected';
            }
            if ($model->getActive() === 1){
                $selectedActive = 'selected';
            }
            if ($model->getActive() === 0){
                $selectedInactive = 'selected';
            }
            $redirectId = $model->getId();
            $showDeleteButton = true;
        }
        require_once GF_REDIRECTS_DIR . 'Packages/MenuPage/view/templates/redirectCreateForm.phtml';
    }

    private function getNav(string $activeItem, $msg = ''): void
    {
        if (isset($msg['success'])){
            $class = 'success';
            $msg = $msg['success'];
        }
        if (isset($msg['error'])){
            $class = 'error';
            $msg = $msg['error'];
        }
        if ($activeItem === 'redirectCreate') {
            $redirectCreate = 'active';
        }
        if ($activeItem === 'redirectList'){
            $redirectList = 'active';
        }
        require GF_REDIRECTS_DIR . 'Packages/MenuPage/view/templates/navigation.phtml';
    }

}