jQuery(document).ready(function (){
    let tableContainer = jQuery('#redirectList');
    if (tableContainer){
        tableContainer.DataTable({
            processing: true,
            serverSide: true,
            searching: true,
            searchDelay: 1500,
            pageLength: 25,
            lengthMenu: [ 10, 25, 50, 75, 100,250,500, 1000 ],
            dom: 'lfriptrip',
            fnInitComplete : function () {
                jQuery('#redirectList tbody tr').on('click',function (){
                    let id =jQuery(this).find('td:first-of-type').text();
                    let currentPage = location.href;
                    currentPage = currentPage.split('&');
                    location.href = currentPage[0] + '&action=redirectCreateForm&redirectId='+id;
                })
            },
            ajax: {
                url: 'admin-ajax.php',
                type: 'POST',
                data:{
                    action:'redirectAdminPage',
                    method:'getAll'
                }
            },
            language: {
                "emptyTable": "Nije pronadjena nijedan pretplatnik sa zadatim filterima"
            },
            columns: [
                {
                    data:'redirectId',
                    name:'redirectId',
                    orderable:false,
                },
                {
                    data: "from",
                    name: "from",
                    orderable:false,
                },

                {
                    data: "to",
                    name: "to",
                    orderable:true,
                },
                {
                    data: "statusCode",
                    name: "statusCode",
                    orderable:false,
                },
                {
                    data: "active",
                    name: "active",
                    orderable:false,
                },
                {
                    data: "userId",
                    name: "userId",
                    orderable:false,
                },
                {
                    data: "createdAt",
                    name: "createdAt",
                    orderable:false,
                },
                {
                    data: "updatedAt",
                    name: "updatedAt",
                    orderable:false,
                }
            ],
        });
    }
    let formContainer = jQuery('#redirectCreateForm');
    if(formContainer){
        formContainer.on('submit',function (e){
            let errors = validateFormInputs();
            let errorsContainer = jQuery('#formErrors');
            if (errors){
                e.preventDefault();
              errorsContainer.empty();
                for (const i in errors) {
                    errorsContainer.append('<p class="error">'+ errors[i]+'</p>')
                }
            }
        })
        jQuery('.deleteButton').on('click', function () {
            formContainer.attr('action','?page=gf-redirects&action=deleteRedirect').submit();
        })
    }

})

function validateFormInputs(){
    let errors = [];
    if (jQuery('input[name="from"]').val().length < 3){
        errors.push('"From" polje ne može biti prazno')
    }
    if (jQuery('input[name="to"]').val().length < 3){
        errors.push('"To" polje ne može biti prazno')
    }
    if (errors.length > 0) {
        return errors;
    }
    return false;
}