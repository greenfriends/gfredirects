<?php


namespace GfRedirects\MenuPage\Service;


use GfRedirects\MenuPage\Controller\MenuPageController;

class MenuPageSetup
{
    private string $pageTitle;
    private string $menuTitle;
    private string $capability;
    private string $menuSlug;
    private MenuPageController $controller;

    /**
     * MenuPage constructor.
     * @param string $pageTitle
     * @param string $menuTitle
     * @param string $capability
     * @param string $menuSlug
     */
    public function __construct(string $pageTitle, string $menuTitle, string $capability, string $menuSlug)
    {
        $this->pageTitle = $pageTitle;
        $this->menuTitle = $menuTitle;
        $this->capability = $capability;
        $this->menuSlug = $menuSlug;
        $this->controller = new MenuPageController();
        $this->controller->setupAjaxHandler();
        $this->createPage();
    }

    private function createPage(): void
    {
        add_action('admin_menu', function (){
            add_submenu_page('nss-panel', $this->pageTitle,$this->menuTitle,$this->capability,
                $this->menuSlug, [$this, 'menuPage']);
        });
    }


    public function menuPage(): void
    {
        echo '<link href="'. GF_REDIRECTS_DIR_URI.'Packages/MenuPage/view/css/redirectsAdmin.css" rel="stylesheet">';
        echo '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.25/b-1.7.1/sl-1.3.3/datatables.min.css"/>';
        $this->controller->handlePageAction($_GET['action'] ?? null);
        echo '<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.25/b-1.7.1/sl-1.3.3/datatables.min.js"></script>';
        echo '<script src="' . GF_REDIRECTS_DIR_URI .'Packages/MenuPage/view/js/redirectsAdmin.js"></script>';
    }
}