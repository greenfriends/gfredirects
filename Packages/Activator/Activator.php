<?php


namespace GfRedirects\Activator;


class Activator
{
    private \wpdb $db;

    private bool $activate = true;

    /**
     * Activator constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
    }

    public function activate(): void
    {
        if ($this->activate){
            $this->createRedirectTable();
            $this->createActiveIndex();
            $this->createUserIdIndex();
        }
    }

    private function createRedirectTable(): void
    {
        $charset_collate = $this->db->get_charset_collate();
        $table_name = $this->db->prefix.REDIRECT_TABLE_NAME;
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
		  `redirectId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `from` varchar(255) NOT NULL,
		  `to` varchar(255) NOT NULL,
		  `statusCode` int(3) UNSIGNED NOT NULL,
		  `active` int(1) DEFAULT 0,
		  `userId` int(10) UNSIGNED DEFAULT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  (redirectId),
		  CONSTRAINT fromUrl UNIQUE (`from`)
		) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

    private function createActiveIndex(): void
    {
        $table_name = $this->db->prefix.REDIRECT_TABLE_NAME;
        $sql = "CREATE index wp_gf_redirects_active_index
	        on $table_name (`active`);";
        $this->db->query($sql);
    }

    private function createUserIdIndex():void
    {
        $table_name = $this->db->prefix.REDIRECT_TABLE_NAME;
        $sql = "CREATE index wp_gf_redirects_userId_index
	        on $table_name (`userId`);";
        $this->db->query($sql);
    }
}