<?php
/*
Plugin Name: GF redirects
Description: Plugin for creating 3.x.x redirects
Version: 1.0.0
Author: Green Friends
License: GPLv2 or later
Text Domain: greenFriends
*/

use GfRedirects\Activator\Activator;
use GfRedirects\MenuPage\Service\MenuPageSetup;
use GfRedirects\Redirect\Service\AutoRedirect;
use GfRedirects\Redirect\Service\PageRedirects;

DEFINE('GF_REDIRECTS_DIR_URI', plugin_dir_url(__FILE__));
DEFINE('GF_REDIRECTS_DIR', __DIR__ . '/');
DEFINE('REDIRECT_TABLE_NAME', 'gf_redirects');

include GF_REDIRECTS_DIR . 'autoloader.php';

register_activation_hook(__FILE__, [new Activator(), 'activate']);
$menuPage = new MenuPageSetup('GF redirects','GF redirects','manage_options','gf-redirects');
$autoRedirect = new AutoRedirect();
$pageRedirects = new PageRedirects();
