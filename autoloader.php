<?php
$dir = new RecursiveDirectoryIterator(GF_REDIRECTS_DIR . '/Packages');
foreach (new RecursiveIteratorIterator($dir) as $file) {
    if (!is_dir($file)) {
        if (fnmatch('*.php', $file)) {
            require $file;
        }
    }
}